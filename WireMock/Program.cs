﻿using System;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using WireMock.Settings;

namespace WireMock
{
    class Program
    {
        static void Main(string[] args)
        {

            // see source code for all the possible properties
            var settings = new FluentMockServerSettings
            {
                Urls = new[] { "http://+:5001", "https://+:5002" },
                StartAdminInterface = true,
                ReadStaticMappings = true,
                WatchStaticMappings = true
            };

            var server = FluentMockServer.Start(settings);
            Console.WriteLine("FluentMockServer running at {0}", string.Join(",", server.Ports));
            server
                .Given(Request.Create().WithPath("/img/healthcareprovider.jpg").UsingGet())
                .RespondWith(Response.Create()
                    .WithStatusCode(200)
                    .WithBodyFromFile("healthcareprovider.jpg")
                );

            server
                .Given(Request.Create().WithPath("/img/healthcareprovider_profile.jpg").UsingGet())
                .RespondWith(Response.Create()
                    .WithStatusCode(200)
                    .WithBodyFromFile("healthcareprovider_profile.jpg")
                );

            server
              .Given(
                Request.Create().WithPath("/some/thing").UsingGet()
              )
              .RespondWith(
                Response.Create()
                  .WithStatusCode(200)
                  .WithHeader("Content-Type", "text/plain")
                  .WithBody("Hello world! Your path is {{request.path}}, your url is {{request.url}}.")
                  .WithTransformer()
              );



            Console.WriteLine("Press any key to stop the server");
            Console.ReadLine();
        }
    }
}
