# CMS Mock

## PMC (Prinses Maxima Centrum)

__OAUTH TOKEN__

url: <host>/pmc/oauth/token

This mock returns an access token.


__CONFIGURATIONS__

url: <host>/pmc/api/configurations

This mock returns a list of all configurations


url: <host>/pmc/api/configurations/2

This mock returns a specific configuration of pmc


__ARTICLES__

url: <host>/pmc/api/articles/2

This mock returns a list of articles based on the configuration 2 (pmc)


_HealthcareProvider_

url: <host>/pmc/api/articles/2/1

img_url's: 
* <host>/img/healthcareprovider.jpg
* <host>/img/healthcareprovider_profile.jpg

This mock returns a page with a healthcareprovider and profile image.


_SubheadersAndScrollingImages_

url: <host>/pmc/api/articles/2/2

This mock returns a page with subheaders and scrolling images


_Dropdown_

url: <host>/pmc/api/articles/2/3

This mock returns a page with a dropdown


_Video_

url: <host>/pmc/api/articles/2/4

This mock returns a page with a link to a Youtube video


_GoogleMaps_

url: <host>/pmc/api/articles/2/5

This mock returns a page with Google Maps map


_BulletPoints_

url: <host>/pmc/api/articles/2/6

This mock returns a page with bullit points
